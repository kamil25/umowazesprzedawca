FROM tomcat:7.0-jre8-slim
COPY ROOT.xml /usr/local/tomcat/conf/Catalina/localhost/ROOT.xml
COPY target/umowazesprzedawca.war /usr/local/tomcat/webapps/umowazesprzedawca.war