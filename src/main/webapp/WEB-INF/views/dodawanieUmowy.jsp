<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<script src="${js}/bootstrap.min.js"></script>
<script src="${js}/bootbox.min.js"></script>
<script>
	
</script>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<div class="container">

	<c:if test="${not empty message}">
		<div class="row">
			<div class="col-xs-12 col-md-offset-2 col-md-8">
				<div class="alert alert-info fade in">${message}</div>
			</div>
		</div>
	</c:if>

	<div class="row">

		<div class="col-md-offset-2 col-md-8">

			<div class="panel panel-primary">

				<div class="panel-heading">

					<h4>Dodaj umowę</h4>

				</div>

				<div class="panel-body">
					<sf:form class="form-horizontal" modelAttribute="umowa"
						action="${contextRoot}/create/umowa" method="POST"
						enctype="multipart/form-data">
						<div class="form-group">
							<label class="control-label col-md-4">Miejscowo&#347;&#263;
								zawarcia umowy</label>
							<div class="col-md-8">
								<sf:input type="text" path="miejscowosc" class="form-control"
									required="true" />
								<sf:errors path="miejscowosc" cssClass="help-block" element="em" />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4">Email
								sprzedaj&#261;cego</label>
							<div class="col-md-8">
								<sf:input type="text" path="sprzedajacyEmail"
									class="form-control" required="true" />
								<sf:errors path="sprzedajacyEmail" cssClass="help-block"
									element="em" />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4">Przedmiot umowy</label>
							<div class="col-md-8">
								<sf:input type="text" path="przedmiotTransakcji"
									class="form-control" required="true" />
								<sf:errors path="przedmiotTransakcji" cssClass="help-block"
									element="em" />
							</div>
							<sf:hidden path="id" />
							<sf:hidden path="code" />
							<sf:hidden path="active" />
						</div>
						<div class="form-group">
							<label class="control-label col-md-4">Link do strony z
								przedmiotem</label>
							<div class="col-md-8">
								<sf:input type="text" path="link" class="form-control"
									required="true" />
								<sf:errors path="link" cssClass="help-block" element="em" />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4">Cena przedmiotu</label>
							<div class="col-md-8">
								<sf:input type="number" path="cena" class="form-control"
									required="true" pattern='[0-9]+(\\.[0-9][0-9]?)?' min="1"
									value="0" step=".01" />
								<sf:errors path="cena" cssClass="help-block" element="em" />
									
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-4">Data</label>
							<div class="col-md-8">
								<sf:input type="text" path="data" class="form-control"
									required="true" />
								<sf:errors path="data" cssClass="help-block" element="em" />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4">Numer konta
								sprzedaj&#261;cego</label>
							<div class="col-md-8">
								<sf:input type="text" path="nrKonta" class="form-control"
									required="true" />
								<sf:errors path="nrKonta" cssClass="help-block" element="em" />
								<div class="text-left">
									<label>forma płatności - przelew</label>
									<sf:hidden path="formaPlatnosci" value="przelew"/>
								</div>
							</div>
						</div>
						
						<div class="form-group">

							<div class="col-md-offset-4 col-md-4">

								<button type="submit" name="submit" value="Save"
									class="btn btn-primary" id="button">Zapisz</button>

							</div>
						</div>

					</sf:form>

				</div>

			</div>

		</div>

	</div>









</div>