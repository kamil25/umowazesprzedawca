<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<section class="instrukcja" style="margin-left:auto; margin-right:auto;text-align:center">
		<div class="container">
		<header>
					

					<h3
						class="mbr-section-title align-center mbr-fonts-style mbr-bold display-2" >
						<span style="font-weight: normal; margin-top:14px;">W dzisiejszych czasach znaczna część naszych zakupów odbywa się za pośrednictwem internetu.</span>
					</h3>
					
					<h3
						class="mbr-section-title align-center mbr-fonts-style mbr-bold display-2" style="margin-top: 3px; 
						">
						<span style="font-weight: normal;">Zawierając transakcję nie mamy jednak pełnej świadomości kto sprzedaje lub kupuje nasze produkty i niejednokrotnie padamy ofiarom oszustów. </span>
					</h3>
					
					
					
					<h4
						class="mbr-section-title align-center mbr-fonts-style mbr-bold display-2" style="margin-top: 29px;">
						
					
					<span style="font-family:'Lobster', cursive; color:#00CCFF;" href="${contextRoot}/home">UmowaZeSprzedawca.pl</span> 
					<span style="font-weight: normal;">to inicjatywa, która w prosty sposób
					wychodzi naprzeciw takim problemom.</span>
					</h4>
					
					<h4
						class="mbr-section-title align-center mbr-fonts-style mbr-bold display-2" style="margin-top: 29px;">
						
					
					<span style="font-weight: normal;">Zawierając w pełni egzekwowalny akt prawny za pośrednictwem umowazesprzedawca.pl możesz być w pełni bezpieczny o swoje pieniądze i mienie.</span>
					</h4>
				
				</header>
			</div>
	</section>
