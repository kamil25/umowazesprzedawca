<script src="${js}/angular.js"></script>
<script src="${js}/jquery.js"></script>
<script src="${js}/jspdf.min.js"></script>
<script src="${js}/jspdf.debug.js"></script>
<script src="${js}/jspdf.customfonts.min.js"></script>
<script src="${js}/default_vfs.js"></script>
<script src="${js}/umowaControllerSpr.js"></script>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>	
<c:set var="contextRoot" value="${pageContext.request.contextPath}" />

</head>
<body>


	<div class="container" data-ng-app="ShoppingApp3"
		data-ng-controller="UmowaControllerSpr as uCtrl">


		<div class="row" data-ng-init="uCtrl.fetchUmowy(umowa.id)">

			<div class="col-md-12">

				<div class="row" style="margin-bottom:15px;margin-top:15px">
					<div class="col-xs-12">
						<div class="mbr-section-text">
							<b 
								id="uwaga" style="color:red" class="mbr-text pt-3 mbr-light mbr-fonts-style align-left mbr-white display-5">
								Wa&#380;ne: przed zaakceptowaniem umowy, upewnij si&#281; czy kupuj&#261;cy w umowie poda&#322; prawid&#322;owe informacje, 
								nast&#281;pnie dodaj swoje dane, a na ko&#324;cu zaakceptuj umow&#281;. Umowa zostanie zawarta po jej zaakceptowaniu 
								przez kupuj&#261;cego.</b>
						</div>

					</div>
				</div>

				<div class="row is-table-row">

					<div class="col-sm-6" data-ng-repeat="umowa in uCtrl.mojeUmowy">


						<div class="thumbnail" style="border: 1px solid #8cb2bc;
    					border-radius: 5px;
    					background-color: #f4f9fa;
    					box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
						
						
						
							<div class="panel-heading" style="background-color:#4582ec;">

							<h4 id="przedmiot" style="text-align: center;color:white;">
									{{umowa.przedmiotTransakcji}}</h4>

							</div>
							
							<div style="padding: 9px;">
							<h5 id="id" style="border:1px solid #8cb2bc;border-radius: 5px; padding:6px;background-color: white;"
							>numer umowy: <span style="color:#14467a">{{umowa.code}}</span></h5>
							<h5 id="link" style="border:1px solid #8cb2bc;border-radius: 5px; padding:6px;background-color: white;"
							>link do przedmiotu: <a href="{{umowa.link}}" target="_blank">{{umowa.link}}</a></h5>
							<h5 id="cena" style="border:1px solid #8cb2bc;border-radius: 5px; padding:6px;background-color: white;"
							>Cena: <span style="color:#14467a">{{umowa.cena}} zł</span></h5>
							<h5 id="formaPlatnosci" style="border:1px solid #8cb2bc;border-radius: 5px; padding:6px;background-color: white;"
							>Forma p&#322;atno&#347;ci: <span style="color:#14467a">{{umowa.formaPlatnosci}}</span></h5>
							<h5 id="kupujacyEmail" style="border:1px solid #8cb2bc;border-radius: 5px; padding:6px;background-color: white;"
							>Email kupuj&#261;cego: <span style="color:#14467a">{{umowa.kupujacyEmail}}</span></h5>
							<h5 id="zaakceptowana{{umowa.id}}" style="border:1px solid #8cb2bc;border-radius: 5px; padding:6px;background-color: white;"
							>Zaakceptowana przez sprzedaj&#261;cego: <button type="button" class="btn btn-primary btn-sm"
								data-ng-click="uCtrl.accept(umowa.id)" id="button">Sprawdź</button></h5>
							<h5 id="zaakceptowanaKupujacy{{umowa.id}}" style="border:1px solid #8cb2bc;border-radius: 5px; padding:6px;background-color: white;"
							>Zaakceptowana przez kupuj&#261;cego: <button type="button" class="btn btn-primary btn-sm"
								data-ng-click="uCtrl.accept(umowa.id)" id="button">Sprawdź</button></h5>
							</div>
							
							<div style="padding: 9px;">
							<button type="button" class="btn btn-primary"
								data-ng-click="uCtrl.pdf3(umowa.id)" id="button2">Pobierz PDF</button>
							
							<a class="btn btn-info"
								href="${contextRoot}/create/umowa/{{umowa.id}}/dodajSprzedajacego">Dodaj moje dane do umowy</a>

							<a class="btn btn-success" id="zaakceptuj{{umowa.id}}" 
								data-ng-click="uCtrl.zaakceptuj(umowa.id)">Zaakceptuj
								umow&#281;</a>
							</div>

						</div>


					</div>



				</div>



			</div>

		</div>

	</div>


</body>
<!-- /.container -->