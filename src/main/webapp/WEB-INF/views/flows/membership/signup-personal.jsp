<%@include file="../flows-shared/header.jsp" %>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>	

<!DOCTYPE html>
<html lang="pl">
<meta charset="UTF-8" />
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>		
</head>
	<div class="container">
		
		
		<div class="row">
			
			<div class="col-md-6 col-md-offset-3">
				
				<div class="panel panel-primary">
				
					<div class="panel-heading">
						<h4>Rejestracja</h4>
					</div>
					
					<div class="panel-body">
										
						<sf:form
							method="POST"
							modelAttribute="user"
							class="form-horizontal"
							id="registerForm"
							accept-charset="utf-8"
						>
						
							
							<div class="form-group">
								<label class="control-label col-md-4">Imię</label>
								<div class="col-md-8">
									<sf:input type="text" path="firstName" class="form-control"
										 />
									<sf:errors path="firstName" cssClass="help-block" element="em"/> 
								</div>
							</div>


							<div class="form-group">
								<label class="control-label col-md-4">Nazwisko</label>
								<div class="col-md-8">
									<sf:input type="text" accept-charset="utf-8" path="lastName" class="form-control"
										/>
									<sf:errors path="lastName" accept-charset="utf-8" cssClass="help-block" element="em"/> 
								</div>
							</div>
						
							<div class="form-group">
								<label class="control-label col-md-4">Email</label>
								<div class="col-md-8">
									<sf:input type="text" path="email" class="form-control"
										 />
									<sf:errors path="email" cssClass="help-block" element="em"/> 									
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-4">Numer kontaktowy</label>
								<div class="col-md-8">
									<sf:input type="text" path="contactNumber" class="form-control"
										 maxlength="15" />
									<sf:errors path="contactNumber" cssClass="help-block" element="em"/> 
								</div>
							</div>
							<!-- nowe pola -->
							
							<div class="form-group">
								<label class="control-label col-md-4">PESEL</label>
								<div class="col-md-8">
									<sf:input type="text" path="pesel" class="form-control"
										 maxlength="15" />
									<sf:errors path="pesel" cssClass="help-block" element="em"/> 
								</div>
							</div>
							
							
							
							<div class="form-group">
								<label class="control-label col-md-4">Has&#322;o</label>
								<div class="col-md-8">
									<sf:input type="password" path="password" class="form-control"
										 pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" 
										title="Haslo musi zawierac conajmniej 8 znakow, jedna cyfre i jedna wielka litere"/>
									<sf:errors path="password" cssClass="help-block" element="em"/> 
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label col-md-4">Powt&#243;rz Has&#322;o</label>
								<div class="col-md-8">
									<sf:input type="password" path="confirmPassword" class="form-control"
										/>
									<sf:errors path="confirmPassword" cssClass="help-block" element="em"/>										 
								</div>
							</div>
							
							
				

							<div class="form-group">
								<div class="col-md-offset-4 col-md-8">
									<button type="submit" name="_eventId_billing" class="btn btn-primary">
										Dalej <span class="glyphicon glyphicon-chevron-right"></span>
									</button>																	 
								</div>
							</div>
						
						
						</sf:form>					
					
					
					</div>
				
				
				</div>
			
			
			</div>
		
		
		</div>
		
		
	</div>

<%@include file="../flows-shared/footer.jsp" %>			
