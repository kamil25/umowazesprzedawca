<%@include file="../flows-shared/header.jsp"%>

<script src="${js}/jquery.js"></script>
<script src="${js}/jquery.validate.js"></script>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<html lang="pl">
<meta charset="UTF-8" />
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
</head>
<div class="container">

	<div class="row">

		<div class="col-sm-6">

			<div class="panel panel-primary">

				<div class="panel-heading">
					<h4>Dane personalne</h4>
				</div>

				<div class="panel-body">
					<div class="text-center">
						<h3>
							<strong>${registerModel.user.firstName}
								${registerModel.user.lastName}</strong>
						</h3>
						<h4>
							<strong>${registerModel.user.email}</strong>
						</h4>
						<h4>
							<strong>${registerModel.user.contactNumber}</strong>
						</h4>
						
						<p>
							<a href="${flowExecutionUrl}&_eventId_personal"
								class="btn btn-primary">Edytuj</a>
						</p>
					</div>
				</div>

			</div>


		</div>

		<div class="col-sm-6">

			<div class="panel panel-primary">

				<div class="panel-heading">
					<h4>Adres</h4>
				</div>

				<div class="panel-body">
					<div class="text-center">
						<p>${registerModel.billing.addressLineOne},</p>
						<p>${registerModel.billing.addressLineTwo},</p>
						<p>${registerModel.billing.city}-
							${registerModel.billing.postalCode},</p>
						<p>${registerModel.billing.state}</p>
						<p>${registerModel.billing.country}</p>
						<p>
							<a href="${flowExecutionUrl}&_eventId_billing"
								class="btn btn-primary">Edytuj</a>
						</p>
					</div>
				</div>

			</div>

		</div>

	</div>

	<div class="row">

		<div class="col-sm-4 col-sm-offset-4">

			<div class="text-center">
				<td width="70%"><input type="checkbox" name="TOS"
					value="Accept" onClick="EnableSubmit(this)"></td>
				<td width="30%">Akceptuję treść <a href="${contextRoot}/regulamin" target="_blank">regulaminu</a></td>
				<td width="10px" style="min-width: 10px"></td>
				<td></td><br/>
				
			</div>
			<div class="text-center" style="margin-top:15px">
				<a href="${flowExecutionUrl}&_eventId_submit"><button type="button" class="btn btn-primary" id="button" disabled
								>zarejestruj się</button></a> 

			</div>

		</div>

	</div>

</div>
<script>

EnableSubmit = function(val)
{
    var sbmt = document.getElementById("button");

    if (val.checked == true)
    {
        sbmt.disabled = false;
    }
    else
    {
        sbmt.disabled = true;
    }
}   


</script>
<%@include file="../flows-shared/footer.jsp"%>