<script src="${js}/myapp.js"></script>


</head>
<body>


<div class="container">

	<div class="row">


		

		<!-- to display the actual products -->
		<div class="col-md-9">

			<!-- Added breadcrumb component -->
			<div class="row">

				<div class="col-lg-12">

					<c:if test="${userClickAllUmowy == true}">
					
						<script>
							window.categoryId = '';
						</script>
					
						<ol class="breadcrumb">


							<li><a href="${contextRoot}/home">Home</a></li>
							<li class="active">All Products</li>


						</ol>
					</c:if>
					
					
					<c:if test="${userClickCategoryProducts == true}">
						<script>
							window.categoryId = '${category.id}';
						</script>
					
						<ol class="breadcrumb">


							<li><a href="${contextRoot}/home">Home</a></li>
							<li class="active">Category</li>
							<li class="active">${category.name}</li>


						</ol>
					</c:if>
					

				</div>


			</div>

			
			<div class="row">
			
				<div class="col-xs-12">
				
					
					<table id="umowaListTable" class="table table-striped table-borderd">
					
					
						<thead>
						
							<tr>
								<th>code</th>
								<th>miejscowosc</th>
								<th>przedmiotTransakcji</th>
								<th>active</th>
								<th></th>
								
								
								
								
								
							
							</tr>
						
						</thead>
					

						<tfoot>
						
							<tr>
								<th>code</th>
								<th>miejscowosc</th>
								<th>przedmiotTransakcji</th>
								<th>active</th>
								<th></th>
								
								
								
								
							
							</tr>
						
						</tfoot>
					</table>
				
				</div>
			
			</div>


		</div>



	</div>


</div>
</body>