<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<spring:url var="css" value="/resources/css" />
<spring:url var="js" value="/resources/js" />
<spring:url var="images" value="/resources/images" />

<c:set var="contextRoot" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html lang="pl">

<head>


<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Umowa ze sprzedawcą - umowa kupna-sprzedaży online - bezpieczne zakupy w internecie">
<meta name="author" content="Kamil Gnadczyński">
<meta name="_csrf" content="${_csrf.token}">
<meta name="_csrf_header" content="${_csrf.headerName}">
<link rel="shortcut icon" type="image/x-icon" href="${images}/favicon.ico" />
<title>umowa kupna-sprzedaży online</title>

<script>
	window.menu = '${title}';
	
	window.contextRoot = '${contextRoot}'
	
</script>

<!-- Bootstrap Core CSS -->
<link href="${css}/bootstrap.min.css" rel="stylesheet">

<!-- Bootstrap Readable Theme -->
<link href="${css}/bootstrap-readable-theme.css" rel="stylesheet">


<!--  <link href="${css}/bootstrap-orange-theme.css" rel="stylesheet">-->

<!-- Bootstrap DataTables -->
<link href="${css}/dataTables.bootstrap.css" rel="stylesheet">


<!-- Custom CSS -->
<link href="${css}/myapp.css" rel="stylesheet">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

	
	<div class="se-pre-con"></div>
	<div class="wrapper">

		<!-- Navigation -->
		<%@include file="./shared/navbar.jsp"%>

		<!-- Page Content -->

		<div class="content">
			
			<!-- Loading the home content -->
			<c:if test="${userClickHome == true }">
				<%@include file="home.jsp"%>
			</c:if>

			<!-- Load only when user clicks about -->
			<c:if test="${userClickAbout == true }">
				<%@include file="about.jsp"%>
			</c:if>

			<!-- Load only when user clicks contact -->
			<c:if test="${userClickContact == true }">
				<%@include file="contact.jsp"%>
			</c:if>
			
			<!-- Load only when user clicks regulamin -->
			<c:if test="${userClickRegulamin == true }">
				<%@include file="regulamin.jsp"%>
			</c:if>
			
			
			<!-- Load only when user clicks dodawanieUmowy -->
			<c:if test="${userClickDodawanieUmowy == true}">
				<%@include file="dodawanieUmowy.jsp"%>
			</c:if>	
			<!-- Load only when user clicks mojeUmowy -->
			<c:if test="${userClickAllUmowy == true}">
				<%@include file="mojeUmowy.jsp"%>
			</c:if>	
			<!-- Load only when user clicks listaUmow -->
			<c:if test="${userClickListaUmow == true}">
				<%@include file="listaUmow.jsp"%>
			</c:if>	
			<!-- Load only when user clicks listaUmow -->
			<c:if test="${userClickListaUmowSprzedajacy == true}">
				<%@include file="listaUmowSprzedajacy.jsp"%>
			</c:if>	
			<!-- Load only when user clicks mojeKonto -->
			<c:if test="${userClickEdycjaUsera == true}">
				<%@include file="edycjaUsera.jsp"%>
			</c:if>	
			<!-- Load only when user clicks skasowane -->
			<c:if test="${userClickSkasowane == true}">
				<%@include file="skasowane.jsp"%>
			</c:if>	
			
			<c:if test="${userClickSsl == true}">
				<%@include file="godaddy.jsp"%>
			</c:if>	
			

		</div>


		<!-- Footer comes here -->
		<%@include file="./shared/footer.jsp"%>

		<!-- jQuery -->
		<script src="${js}/jquery.js"></script>

		<script src="${js}/jquery.validate.js"></script>

		<!-- Bootstrap Core JavaScript -->
		<script src="${js}/bootstrap.min.js"></script>
		
		<!-- DataTable Plugin -->
		<script src="${js}/jquery.dataTables.js"></script>
		
		<!-- DataTable Bootstrap Script -->
		<script src="${js}/dataTables.bootstrap.js"></script>
		
		<!-- DataTable Bootstrap Script -->
		<script src="${js}/bootbox.min.js"></script>
		
		<!-- Self coded javascript -->
		<script src="${js}/myapp.js"></script>

	</div>

</body>

</html>