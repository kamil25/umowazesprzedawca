<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<div class="container">

	<div class="row">	
		
		
		
		<h3>REGULAMIN KORZYSTANIA Z USŁUG APLIKACJI UMOWAZESPRZEDAWCA
przyjęty w dniu 23.10.2018 r.</h3>
 
§ 1 Definicje
 
Na potrzeby niniejszego Regulaminu wskazane poniżej pojęcia będą miały następujące znaczenie:
1.	Aplikacja - Aplikacja UmowaZeSprzedawca umożliwiająca, w oparciu o zaproponowane w jej zasobach Wzór Dokumentu, składanie Kontrahentowi prawnie wiążących oświadczeń woli o treści uzupełnionej przez Użytkownika; 
2.	Użytkownik - osoba fizyczna, która ukończyła 18 lat i posiada pełną zdolność do czynności prawnych;
3.	Kontrahent -  osoba fizyczna, która ukończyła 18 lat i posiada pełną zdolność do czynności prawnych;
4.	Konto - zindywidualizowany zapis informatyczny utworzony przez Usługodawcę na rzecz Użytkownika w celu korzystania z usług świadczonych w Aplikacji oraz gromadzenia w tym celu niezbędnych informacji. Dostęp do Konta zapewnia login, którym jest adres e-mail podany podczas rejestracji i hasło;
5.	Umowa - umowa łącząca Użytkownika i Usługodawcę zawierająca istotne warunki świadczenia usług przez Usługodawcę polegających na umożliwieniu przy wykorzystaniu Aplikacji składania Kontrahentowi prawnie wiążących oświadczeń woli, której postanowienia wraz z postanowieniami Regulaminu stanowią integralną całość i składają się na powstanie zobowiązania cywilnoprawnego;
6.	Umowa UmowaZeSprzedawca - umowa zawarta przy użyciu Aplikacji, której stronami są Użytkownik i Kontrahent;
7.	Dokumenty UmowaZeSprzedawca - umowy zawarte przy użyciu Aplikacji i oświadczenia woli złożone przy użyciu Aplikacji, o których mowa w ust. 6 niniejszego paragrafu;
8.	Strony - należy przez to rozumieć łącznie Usługodawcę i Użytkownika, którzy zawarli ze sobą Umowę;
9.	Strony Dokumentów UmowaZeSprzedawca - należy przez to rozumieć łącznie Użytkownika i Kontrahenta, którzy zawarli ze sobą Umowę UmowaZeSprzedawca
10.	Usługodawca – umowa ze sprzedawca;
11.	Wzory Dokumentów - dostępny w bazie Aplikacji szablon Dokumentu UmowaZeSprzedawca, przy pomocy którego Użytkownik formułuje treść Dokumentów UmowaZeSprzedawca;
12.	Regulamin - niniejszy regulamin.
 
§ 2 Postanowienia ogólne
 
1.	Niniejszy regulamin określa zasady korzystania z Aplikacji w wersji Web przeznaczonej do używania w przeglądarkach internetowych.
2.	Regulamin o którym mowa w ustępie powyżej jest regulaminem w rozumieniu art. 8 ustawy z dnia 18 lipca 2002 r. o świadczeniu usług drogą elektroniczną (t.j. Dz. U. z 2017 r., poz. 1219).
3.	Właścicielem i operatorem Aplikacji, a także usługodawcą świadczonych za jej pośrednictwem usług jest spółka umowa ze sprzedawca .
4.	Usługi świadczone za pośrednictwem Aplikacji polegają na usprawnianiu składania oświadczeń woli w obrocie prawnym. Aplikacja umożliwia, w oparciu o zaproponowane przez Aplikację Wzory Dokumentów, składanie Kontrahentowi  prawnie wiążących oświadczeń woli o treści wprowadzonej przez Użytkownika.
5.	Oświadczenie woli złożone w formie dokumentowej za pomocą Aplikacji jest równoważne w obrocie prawnym z oświadczeniem woli złożonym w formie pisemnej, o ile do jego złożenia ustawodawca nie przewidział wymogu zachowania innej formy oświadczenia woli pod rygorem nieważności. Wyrażając zgodę na złożenie oświadczenia woli Użytkownik potwierdza, że podał prawdziwe dane osobowe oraz adresowe. Oznacza to, że po kliknięciu przycisku “Zaakceptuj umowę” powstaje stosunek zobowiązaniowy o treści zgodnej z treścią oświadczenia woli, co może wiązać się dla Użytkownika z odpowiedzialnością majątkową wynikającą ze złożenia oświadczenia woli.
6.	Usługodawca rekomenduje odpowiedzialne korzystanie z Aplikacji, ze świadomością możliwych konsekwencji zaciągniętych zobowiązań oraz zastrzega, że nie ma wpływu na treść oświadczeń woli składanych za pośrednictwem Aplikacji, a jedynie umożliwia przekazywanie oświadczeń woli pomiędzy Użytkownikami oraz udostępnia Wzory Dokumentów.
7.	Użytkownicy decydując się na złożenie oświadczenia woli za pomocą Aplikacji mają obowiązek przed złożeniem oświadczenia woli uważnie przeczytać treść projektu Dokumentu UmowaZeSprzedawca wygenerowanego przez Aplikację oraz zrozumieć znaczenie jego zapisów.  Zawierając Umowę UmowaZeSprzedawca, Użytkownicy wyrażają zgodę na wszystkie zapisy Wzoru Dokumentu w związku z tym w przypadku gdy jakikolwiek zapis Wzoru Dokumentu lub Umowy UmowaZeSprzedawca jest dla użytkowników niezrozumiały należy odstąpić od jej zawierania lub składania oświadczenia woli danej treści.
8.	Użytkownik Aplikacji zakładając Konto w Aplikacji wyraża zgodę na otrzymywanie ofert zawarcia Umowy UmowaZeSprzedawca od innych Użytkowników.
9.	Użytkownik Aplikacji zakładając Konto w Aplikacji wyraża zgodę na otrzymywanie ofert handlowych od Usługodawcy.
10.	Użytkownicy Aplikacji są zobowiązani do korzystania z Aplikacji w sposób zgodny z obowiązującym prawem i Regulaminem.
11.	Użytkownicy Aplikacji są zobowiązani w szczególności do korzystania z Aplikacji w sposób niezakłócający jej funkcjonowania oraz korzystania z Aplikacji w sposób nieuciążliwy dla innych użytkowników oraz Usługodawcy, z poszanowaniem dóbr osobistych osób trzecich (w tym prawa do prywatności) i wszelkich innych przysługującym im praw, a także korzystania z wszelkich informacji i materiałów udostępnionych za pośrednictwem Aplikacji jedynie w zakresie dozwolonego użytku.
12.	Użytkownicy Aplikacji są zobowiązani niezwłocznie powiadomić Usługodawcę o każdym przypadku naruszenia ich praw w związku z korzystaniem z Aplikacji.
 
 
§ 3 Wymogi techniczne
 
1.	Aplikacja dostępna jest dla użytkowników przeglądarek internetowych:
a.	Chrome 54,
b.	Chrome Mobile - wersja z 24.10.2016 r.
c.	Firefox 45,
d.	Internet Explorer 11.
6.	Koszty transmisji danych wymaganych do korzystania z Aplikacji pokrywają jej Użytkownicy we własnym zakresie na podstawie umów zawartych z operatorami telekomunikacyjnymi lub innym dostawcą Internetu. Usługodawca nie ponosi odpowiedzialności za wysokość opłat naliczanych z tytułu wykorzystania transmisji danych niezbędnego do korzystania z Aplikacji. Usługodawca zaleca użytkownikom Aplikacji korzystanie z aplikacji lub funkcji systemu operacyjnego polegających na pomiarze przesyłanych danych.
 
§ 4 Rejestracja Użytkownika
 
1.	Warunkiem dostępu do zasobów Aplikacji jest rejestracja Użytkownika.
2.	Formularz rejestracyjny dostępny jest w domenie www.UmowaZeSprzedawca.pl
3.	Dokonanie rejestracji Użytkownika jest równoznaczne z utworzeniem Konta w Aplikacji i zawarciem Umowy pomiędzy Usługodawcą a Użytkownikiem na świadczenie usług przez Usługodawcę na zasadach określonych w niniejszym Regulaminie.
4.	Warunkiem prawidłowej rejestracji jest wypełnienie obowiązkowych pól wskazanych w formularzu, zaakceptowanie Regulaminu i kliknięcie w pole “Zarejestruj”.
5.	W procedurze rejestracji należy podać jako obowiązkowe następujące dane:
a.    imię,
b.    nazwisko,
c.     e-mail,
d.    hasło.
6.	Hasło musi zawierać co najmniej 8 znaków, jedną dużą literę i jedną cyfrę.
7.	Po prawidłowym uzupełnieniu formularza rejestracyjnego i kliknięciu w pole “Zarejestruj”, wprowadzone dane są przesyłane automatycznie do Usługodawcy w celu utworzenia Konta.
8.	Użytkownik otrzymuje na podany w formularzu rejestracyjnym adres e-mail kod aktywacyjny.
9.	Wpisanie przesłanego kodu aktywacyjnego w polu “Aktywacja Konta” kończy rejestrację i oznacza utworzenie Konta Użytkownika w Aplikacji UmowaZeSprzedawca.
12.	Dla skuteczności działania Aplikacji niezbędne jest podanie prawdziwych danych osobowych. Aplikacja automatycznie uzupełnia oświadczenia woli Użytkownika składane za pomocą Aplikacji o dane osobowe podane przy pierwszym uruchomieniu, z uwzględnieniem prawnych wymogów zastrzeżonych dla skuteczności czynności prawnej.
A.    W przypadku osób fizycznych niezbędne jest podanie następujących danych osobowych:
a.	imię i nazwisko,
b.	adres zamieszkania,
c.	PESEL,
B. W przypadku osób fizycznych prowadzących jednoosobową działalność gospodarczą niezbędne jest podanie następujących danych osobowych:
a.	imię i nazwisko,
b.	 adres zamieszkania,
c.	PESEL,
d.	seria i numer dowodu osobistego,
e.	organ wydający dowód osobisty,
f.	firma działalności gospodarczej,
g.	adres prowadzonej działalności gospodarczej,
h.	numer identyfikacyjny NIP,
i.	numer REGON.
 
§ 5 Logowanie Użytkownika
 
1.	Użytkownik, który dokonał rejestracji Konta w sposób opisany w paragrafie poprzedzającym korzysta z zasobów Aplikacji po zalogowaniu się w domenie htttp://UmowaZeSprzedawca.pl/aplikacja
2.	W celu zalogowania się do Aplikacji Użytkownik wpisuje e-mail oraz hasło podane przy rejestracji i wybiera pole “Zaloguj się”.
3.	Użytkownik, który nie pamięta hasła może skorzystać z funkcji “Przypomnij mi” i po podaniu adresu e-mail, który podał podczas rejestracji jako e-mail wykorzystywany na potrzeby Aplikacji, otrzyma na podaną skrzynkę mailową klucz przywracający hasło.
 
§ 6 Dezaktywacja Konta
 
1.	Użytkownik może w każdym czasie żądać dezaktywacji swojego Konta.
2.	W przypadku o którym mowa w ust. 1 Użytkownik wysyła żądanie dezaktywacji Konta za pośrednictwem formularza dostępnego w Aplikacji.Formularz jest dostępny pod adresem https://www.UmowaZeSprzedawca.pl/mojeKonto/
3.	Dezaktywacja Konta wiąże się z utratą możliwości zawierania Dokumentów UmowaZeSprzedawca za pośrednictwem Aplikacji, aż do momentu powtórnej aktywacji Konta.
4.	Dezaktywacja Konta nie ma wpływu na obowiązywanie zawartych Umów oraz złożonych oświadczeń woli za pośrednictwem Aplikacji, tj. pozostają one obowiązujące zgodnie z ich treścią.
 

 
§ 8 Procedura zawierania Dokumentów UmowaZeSprzedawca
 
1.	Użytkownik przystępuje do przygotowania treści Dokumentu UmowaZeSprzedawca poprzez kliknięcie w pole o nazwie „Dodaj umowę”.
2.	Użytkownika przystępując do tworzenia Dokumentu automatycznie staję się stroną kupna.
3.	Połączenie Użytkownika z Kontrahentem i umożliwienie im zawarcia Umowy UmowaZeSprzedawca następuje poprzez podanie adresu email Kontrahenta
5.	Gdy Użytkownik uzna, że wprowadzone przez niego dane są poprawne, przesyła Dokument UmowaZeSprzedawca do Kontrahenta poprzez kliknięcie w przycisk “Zapisz”.
6.	Przesłanie Umowy UmowaZeSprzedawca do Kontrahenta w sposób opisany w ustępie 5 powyżej stanowi ofertę. Zaakceptowanie treści Umowy UmowaZeSprzedawca przez Kontrahenta w brzmieniu niezmienionym oznacza przyjęcie oferty przez Kontrahenta.
7.	Termin związania ofertą wynosi każdorazowo 48 godzin licząc od momentu jej przesłania.
9.	Każdorazowo o otrzymaniu ofert Użytkownik oraz Kontrahent powiadamiani są drogą mailową na adresy wskazane podczas rejestracji Konta.
10.	Dla skutecznego zawarcia umowy UmowaZeSprzedawca wymaga się dokonania akceptacji jego treści zarówno przez Kontrahenta jak i użytkownika, poprzez kliknięcie w przycisk „zaakceptuj umowę”.
 
§ 12 Dostęp Użytkownika do Dokumentów UmowaZeSprzedawca
 
1.	W zakładce “Lista umów - kupno” oraz „Lista umów – sprzedaż” Użytkownikowi umożliwia się dostęp do pełnego wykazu zawartych przez niego za pomocą Aplikacji Umów UmowaZeSprzedawca, projektów Umów UmowaZeSprzedawca, których formularze znajdują się w trakcie uzupełniania, Umów UmowaZeSprzedawca oczekujących na akceptację przez Kontrahenta.
 
§ 14 Prawa autorskie i pokrewne
 
1.	Wszelkie prawa do Aplikacji oraz do udostępnianych treści w całości i we fragmentach, w szczególności elementów tekstowych, graficznych oraz elementów aplikacji programistycznych generujących i obsługujących Aplikację, są zastrzeżone dla Usługodawcy.
2.	Usługodawca udziela Użytkownikowi licencji na korzystanie z treści na zasadach i w czasie określonym w § 14 niniejszego Regulaminu.
3.	Licencja, o której mowa w ust. 2 i 3 nie upoważnia Użytkownika do udzielania dalszych licencji.
4.	Przekazywanie osobom trzecim treści zawartych w Aplikacji dopuszczalne jest przy wykorzystaniu narzędzi zawartych w Aplikacji i przeznaczonych do tego celu.
5.	Użytkownik nie ma prawa zwielokrotniać, sprzedawać lub w inny sposób wprowadzać do obrotu lub rozpowszechniać Aplikacji, w całości bądź we fragmentach, w szczególności przesyłać lub udostępniać jej w systemach i sieciach komputerowych, systemach dystrybucji aplikacji mobilnych lub jakichkolwiek innych systemach teleinformatycznych.
 
§ 15 Zasady udostępniania treści
 
1.	Usługodawca nie gwarantuje nieprzerwanej dostępności treści ani parametrów jakości dostępu do nich. Parametry jakości dostępu są uzależnione od przepustowości internetowej sieci pośredniczącej pomiędzy Usługodawcą a Użytkownikiem i Kontrahentem oraz innych czynników niezależnych od Usługodawcy.
2.	Usługodawca zastrzega sobie prawo ograniczenia, zawieszania lub zaprzestania prezentacji treści w części lub w całości.
3.	Korzystanie z Aplikacji lub treści w sposób sprzeczny z prawem, zasadami współżycia społecznego, dobrymi obyczajami lub niniejszym Regulaminem uzasadnia natychmiastowe zaprzestanie świadczenia Usługi dla danego Użytkownika.
4.	Reklamacje z tytułu nieprawidłowego dostępu do Usług należy zgłaszać Usługodawcy w trybie określonym w § 16 niniejszego Regulaminu.
 
§ 16 Reklamacje
 
1.	Reklamacje dotyczące funkcjonowania Aplikacji i dostępu do jej treści należy zgłaszać drogą mailową na adres Usługodawcy umowazesprzedawca@gmail.com
2.	Reklamacja powinna zawierać dane umożliwiające rozpatrzenie reklamacji, w szczególności imię i nazwisko zgłaszającego oraz adres e-mail podany przy rejestracji Konta, a także wskazywać okoliczności uzasadniające reklamację.
3.	Usługodawca ustosunkuje się do treści reklamacji w ciągu 14 dni za pośrednictwem wiadomości e-mail przesłanej na adres wskazany w reklamacji.
4.	Usługodawca nie rozpoznaje zastrzeżeń niezwiązanych z funkcjonowaniem Aplikacji, w szczególności uwag odnoszących się do niewykonania bądź nienależytego wykonania Umowy UmowaZeSprzedawca przez Kontrahenta. Reklamacje, których przedmiotem są okoliczności wskazane w zdaniu poprzednim, Usługodawca ma prawo pozostawić bez odpowiedzi.
 
§ 17 Umowa
 
1.	Z chwilą dokonania rejestracji w Aplikacji pomiędzy Użytkownikiem a Usługodawcą zostaje zawarta Umowa na czas nieokreślony.
2.	Usługodawca zobowiązuje się do świadczenia usług na rzecz Użytkownika polegających na umożliwieniu mu:
a.	tworzenia projektu Umowy UmowaZeSprzedawca na bazie Wzoru dokumentu i wprowadzonych przez Użytkownika Aplikacji UmowaZeSprzedawca indywidualnych danych;
b.	zawarcia Umowy UmowaZeSprzedawca za pośrednictwem Aplikacji;
3.	Użytkownik zobowiązuje się do:
a.	podania w procesie rejestracji prawdziwych danych;
b.	korzystania z Aplikacji w sposób niezakłócający jej funkcjonowania;
c.	korzystania z Aplikacji w sposób nieuciążliwy dla innych użytkowników oraz Usługodawcy, z poszanowaniem dóbr osobistych osób trzecich (w tym prawa do prywatności) i wszelkich innych przysługującym im praw;
d.	korzystania z wszelkich informacji i materiałów udostępnionych za pośrednictwem Aplikacji jedynie w zakresie dozwolonego użytku.
4.	Użytkownik może w każdej chwili zakończyć korzystanie z usług świadczonych przez Usługodawcę poprzez Dezaktywację Konta.
5.	Użytkownik, który w sposób wskazany w ust. 1 zawarł Umowę z Usługodawcą, może od niej odstąpić w terminie 14 dni od zawarcia Umowy, pod warunkiem, że nie rozpoczął jeszcze korzystania z usług świadczonych w Aplikacji.
6.	Złożenie oświadczenia o odstąpieniu od Umowy dokonane z naruszeniem ust. 6 nie wywołuje skutków prawnych.
 
 
 
 Załącznik Nr 1 - Polityka prywatności
 
§ 1 Postanowienia ogólne
 
1.	Niniejsza Polityka Prywatności (zwana dalej „Polityką Prywatności”) określa sposób zbierania, przetwarzania i przechowywania danych osobowych koniecznych do realizacji usług świadczonych za pośrednictwem Aplikacji umowazesprzedawca (zwanej dalej „Aplikacją”).
2.	Administratorem danych osobowych gromadzonych przez Aplikację jest umowa ze sprzedawca …  (zwany dalej “Administratorem”)
3.	Na potrzeby niniejszej Polityki Prywatności Użytkownikiem jest każdy podmiot korzystający z usług świadczonych za pośrednictwem Aplikacji.
4.	Użytkownik przyjmuje do wiadomości, że udostępnianie przez niego danych osobowych jest dobrowolne. Udostępnianie Administratorowi danych osobowych przez Użytkownika nastąpi po zaakceptowaniu Polityki Prywatności podczas rejestracji w Aplikacji.
5.	Wszystkie dane osobowe są gromadzone, przechowywane i przetwarzane zgodnie z wymogami obowiązującego prawa.
6.	Udostępnienie danych osobowych przez Użytkownika jest niezbędne w celu jego weryfikacji, autoryzacji i prawidłowej obsługi przez Usługodawcę.
7.	Dane osobowe, udostępniane przez Użytkownika, mogą być wykorzystywane przez Administratora do wysyłania Użytkownikowi informacji handlowych o nowościach i ofertach Administratora, wyłącznie w sytuacji gdy Użytkownik wyraził zgodę na otrzymywanie tego typu informacji.
8.	Przekazane dane są chronione przed dostępem do nich osób trzecich i wykorzystywane przez Administratora wyłącznie do kontaktów z Użytkownikami, niezbędnych w ramach funkcjonowania Aplikacji.
9.	Poprzez instalację Aplikacji i założenie Konta Użytkownika, Użytkownik wyraża zgodę na przetwarzanie danych osobowych zgodnie z przepisami ustawy o ochronie danych osobowych i akceptuje Regulamin oraz Politykę Prywatności.
 
§ 2 Dane zbierane automatycznie
 
1.	Administrator nie zbiera bez zgody Użytkownika danych osobowych, a wyłącznie dane nie posiadające takiego przymiotu, w szczególności dane demograficzne i dane dotyczące użytkowania Aplikacji. Zbieranie danych opisanych w zdaniu poprzednim odbywa się automatycznie (zwane dalej „dane zbierane automatycznie”).
2.	Dane zbierane automatycznie nie umożliwiają jednoznacznej identyfikacji Użytkownika.
3.	Dane zbierane automatycznie mogą służyć Administratorowi do poprawy jakości świadczonych usług, w szczególności w przypadku wystąpienia błędu Aplikacji. W sytuacji opisanej powyżej, dane zbierane automatycznie będą dotyczyły błędu Aplikacji, w tym stanu urządzenia mobilnego Użytkownika w chwili wystąpienia błędu, identyfikacji urządzenia mobilnego Użytkownika, fizycznej lokalizacji urządzenia mobilnego Użytkownika w chwili wystąpienia błędu.
4.	Nie ma możliwości zmiany bądź usunięcia danych zbieranych automatycznie.
 
§ 3 Dane zbierane w celu nawiązania kontaktu
 
1.	W przypadkach kontaktowania się Użytkownika z Administratorem, Administrator będzie wymagał podania przez Użytkownika: imienia, nazwiska oraz adresu e-mail, (zwane dalej: „dane zbierane w celu nawiązania kontaktu”).
2.	Podanie przez Użytkownika danych zbieranych w celu nawiązania kontaktu jest dobrowolne, jednakże stanowić będzie wyłączną podstawę nawiązania kontaktu zwrotnego Administratora z Użytkownikiem oraz umożliwi Administratorowi weryfikację Użytkownika.
3.	Dane zbierane w celu nawiązania kontaktu będą wykorzystywane wyłącznie w celu umożliwienia poprawnej, pełnej i sprawnej komunikacji pomiędzy Administratorem a Użytkownikiem.
 
§ 4 Zbieranie danych osobowych
 
1.	W trakcie rejestracji w Aplikacji i korzystania z Aplikacji, Administrator może domagać się podania przez Użytkownika danych osobowych, w celu realizacji usług świadczonych przez Administratora za pośrednictwem Aplikacji.
2.	Dane osobowe Użytkownika zbierane w sposób określony w ust. 1 powyżej obejmują m.in.: imię i nazwisko, adres e-mail, PESEL, adres zamieszkania.
 
§ 5 Przetwarzanie danych osobowych
 
1.	Zebrane dane osobowe Użytkownika będą wykorzystywane przez Administratora celem świadczenia przez Administratora usługi na rzecz Użytkownika.
2.	Dane osobowe Użytkownika mogą być przetwarzane poprzez ich udostępnienie drugiej stronie Umowy umowazesprzedawca dla celów związanych z jej zawarciem.
 
§ 6 Prawa i obowiązki Administratora
 
1.	Administrator zobowiązuje się przetwarzać dane osobowe Użytkownika z zachowaniem wymogów Ustawy z dnia 29 sierpnia 1997 roku o ochronie danych osobowych oraz Ustawy z dnia 18 lipca 2002 roku o świadczeniu usług drogą elektroniczną.
2.	Administrator gwarantuje zapewnienie odpowiednich środków technicznych i organizacyjnych zapewniających bezpieczeństwo przetwarzanych danych osobowych, w szczególności uniemożliwiających dostęp do nich nieuprawnionym osobom trzecim, lub ich przetwarzania z naruszeniem przepisów powszechnie obowiązującego prawa, zapobiegających utracie danych osobowych, ich uszkodzeniu lub zniszczeniu.
3.	Dane osobowe Użytkownika będą przechowywane tak długo, jak będzie to konieczne do realizacji przez Administratora usług świadczonych za pośrednictwem Aplikacji.
4.	Administrator ma prawo udostępniania danych osobowych Użytkownika właściwym organom, które zgłoszą konieczność udostępnienia danych osobowych w oparciu o odpowiednie podstawy powszechnie obowiązującego prawa.
 
§ 7 Prawa i obowiązki Użytkownika
 
1.	Użytkownik ma prawo dostępu do swoich danych osobowych za pośrednictwem Aplikacji.
2.	Użytkownik w każdej chwili ma możliwość usunięcia udostępnionych danych osobowych, za pośrednictwem narzędzi dostępnych w Aplikacji.
3.	W przypadku trwałego usunięcia przez Użytkownika danych osobowych, koniecznych do realizacji przez Administratora usług świadczonych za pośrednictwem Aplikacji, Użytkownik utraci możliwość korzystania z tych usług.
4.	Administrator zastrzega sobie prawo wprowadzenia zmian w Polityce Prywatności, o czym poinformuje Użytkownika za pośrednictwem Aplikacji. Jeżeli Użytkownik nie wyrazi zgody na wprowadzone zmiany, zobowiązany jest trwale usunąć Aplikację ze swojego urządzenia.
 
 


</p>	
	
	</div>


</div>