    <%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
    <div class="container footer">

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="panel-footer">
       				<div class="nav navbar-nav">
                    <li id="about">
                        <a href="${contextRoot}/about">O aplikacji</a>
                    </li>

                    <li id="contact">
                        <a href="${contextRoot}/contact">Kontakt</a>
                    </li>
                    <li id="regulamin">
                        <a href="${contextRoot}/regulamin">Regulamin</a>
                    </li>
                    </div>
                    <div class="text-right">
                    </br>
                    <p>Wszelkie prawa zastrzeżone &copy; Umowa ze sprzedawcą 2018</p>
                    </div>
                </div>
            </div>
        </footer>

    </div>