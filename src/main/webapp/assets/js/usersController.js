var app4 = angular.module('ShoppingApp4', []);

app4.controller('UsersController', function($http) {
	
	
	
	var me = this;
		
	me.mojeKonto = [];
	me.mojeKontoAdres = [];
	
	
	me.fetchMojeKonto = function() {
		
		
		$http.get('/json/data/all/userDane')
			.then(function(response) {
				me.mojeKonto = response.data;
				
		});
			
				
	}
	
	me.fetchMojeKontoAdres = function() {
		
		
		$http.get('/json/data/all/userDaneAdres')
			.then(function(response) {
				me.mojeKontoAdres = response.data;
				
		});
			
				
	}
	
	
	
});

