package com.kmg.umowazesprzedawca.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kmg.umowabackend.dao.AddressDAO;
import com.kmg.umowabackend.dao.UmowaDAO;
import com.kmg.umowabackend.dao.UserDAO;
import com.kmg.umowabackend.dto.Address;
import com.kmg.umowabackend.dto.Umowa;
import com.kmg.umowabackend.dto.User;

@Controller
@RequestMapping("/json/data")
public class JsonDataController {

	
	
	@Autowired
	private UmowaDAO umowaDAO;
	
	private User user;
	
	
	@Autowired
	private UserDAO userDAO;
	
	@Autowired
	private AddressDAO addressDAO;
	
	
	@RequestMapping("/all/umowy")
	@ResponseBody
	public List<Umowa> getAllUmowy() {
		
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		
		
		user = userDAO.getByEmail(authentication.getName());
		return umowaDAO.list();
				
	}
	
	@RequestMapping("/all/userUmowy")
	@ResponseBody
	public List<Umowa> getAllUserUmowy() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		
		
		user = userDAO.getByEmail(authentication.getName());
		
		return umowaDAO.listUserUmowy(user.getId());
		
				
	}
	
	@RequestMapping("/all/userUmowySprzedajacy")
	@ResponseBody
	public List<Umowa> getAllUserUmowySprzedajacy() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		
		
		user = userDAO.getByEmail(authentication.getName());
		
		return umowaDAO.listUserUmowySprzedajacy(user.getEmail());
		
				
	}
	
	@RequestMapping("/all/userDane")
	@ResponseBody
	public List<User> getUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		
		
		user = userDAO.getByEmail(authentication.getName());
		String userEmail = user.getEmail();
		
		return userDAO.listUser(userEmail);
		
				
	}
	
	@RequestMapping("/all/userDaneAdres")
	@ResponseBody
	public List<Address> getUserAdres() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		
		
		user = userDAO.getByEmail(authentication.getName());
		int userId = user.getId();
		
		return addressDAO.getAddressUserId(userId);
		
				
	}
	
	
	
}
