package com.kmg.umowazesprzedawca.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.kmg.umowabackend.dao.UserDAO;
import com.kmg.umowabackend.dto.Umowa;
import com.kmg.umowabackend.dto.User;

@Controller
@RequestMapping("/mojeKonto")
public class UserEditController {
	
	private static final Logger logger = LoggerFactory.getLogger(UserEditController.class);
	
	@Autowired
	private UserDAO userDAO;
	private User user;
	
	@RequestMapping("/")
	public ModelAndView mojeKonto() {		

		
		
		
		ModelAndView mv = new ModelAndView("page");	
		mv.addObject("title","Edycja Usera");		
		mv.addObject("userClickEdycjaUsera",true);
		
		
		
		
			
			
		return mv;
		
	}
	
	@RequestMapping(value = "/skasujKonto/", method=RequestMethod.GET)
	@ResponseBody
	public ModelAndView confirmUserEmail(HttpServletRequest request, HttpServletResponse response) {		
			
		ModelAndView mv= new ModelAndView("page");
		mv.addObject("title", "Skasowane");
		mv.addObject("userClickSkasowane",true);
		
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		user = userDAO.getByEmail(authentication.getName());
	    
		
	    user.setEnabled(false);
	    userDAO.update(user);
	    if (authentication != null){    
	        new SecurityContextLogoutHandler().logout(request, response, authentication);
	    }
		
		return mv;
			
		
	}

}
