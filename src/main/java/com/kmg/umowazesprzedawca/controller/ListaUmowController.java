package com.kmg.umowazesprzedawca.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;





@Controller
public class ListaUmowController {

	private final static Logger logger = LoggerFactory.getLogger(ListaUmowController.class);
	
	
	
		
		
	@RequestMapping(value = "/listaUmow")
	public ModelAndView showListaUmow(@RequestParam(name="success",required=false)String success) {		
			
	ModelAndView mv = new ModelAndView("page");		
	mv.addObject("title","Lista Umow");
	
	logger.info("Inside ListaUmowController index method - INFO");
	logger.debug("Inside ListaUmowController index method - DEBUG");
	
			
	mv.addObject("userClickListaUmow",true);
	return mv;	
			
			
	}
	
	@RequestMapping(value = "/listaUmowSprzedajacy")
	public ModelAndView showListaUmowSprzedajacy(@RequestParam(name="success",required=false)String success) {		
			
	ModelAndView mv = new ModelAndView("page");		
	mv.addObject("title","Lista Umow Sprzedajacy");
	
	logger.info("Inside ListaUmowController index method - INFO");
	logger.debug("Inside ListaUmowController index method - DEBUG");
	
			
	mv.addObject("userClickListaUmowSprzedajacy",true);
	return mv;	
			
	}
		
		
		
		
	
}