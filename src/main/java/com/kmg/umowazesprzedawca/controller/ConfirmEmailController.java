package com.kmg.umowazesprzedawca.controller;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.kmg.umowabackend.dao.UserDAO;
import com.kmg.umowabackend.dto.Umowa;
import com.kmg.umowabackend.dto.User;
import com.kmg.umowabackend.mail.EmailDoKupujacego;

@Controller
@RequestMapping("/confirm")
public class ConfirmEmailController {
	
	private static final Logger logger = LoggerFactory.getLogger(ConfirmEmailController.class);
	
	@Autowired
	private UserDAO userDAO;
	private User user;
	
	
	@RequestMapping(value = "/registry/activation/{username}/{code}", method=RequestMethod.GET)
	@ResponseBody
	public ModelAndView confirmUserEmail(@PathVariable String username, @PathVariable String code) {		
			
		ModelAndView mv= new ModelAndView("login");
		mv.addObject("title", "Login");
		
			user = userDAO.getByEmail(username);
			
			String userCode = user.getCode();
			
			if(userCode.equals(code)) {
				user.setEnabled(true);
				user.setCode("PL" + UUID.randomUUID().toString().substring(26).toUpperCase());
				userDAO.update(user);
				return mv;
			}
			else return mv;
			
		
	}
	
	@RequestMapping(value="/email")
	public ModelAndView login(@RequestParam(name="error", required = false)	String error,
			@RequestParam(name="logout", required = false) String logout) {
		ModelAndView mv= new ModelAndView("confirmEmail");
		mv.addObject("title", "ConfirmEmail");
		if(error!=null) {
			mv.addObject("message", "Email lub hasło użytkownika są niepoprawne!");
		}
		if(logout!=null) {
			mv.addObject("logout", "Zostałeś wylogowany!");
		}
		return mv;
	}

}
