package com.kmg.umowabackend.dao;

import java.util.List;

import com.kmg.umowabackend.dto.Address;

import com.kmg.umowabackend.dto.User;

public interface UserDAO {

	// user related operation
	User getByEmail(String email);
	User get(int id);

	boolean add(User user);
	boolean update(User user);
	
	// adding and updating a new address
	Address getAddress(int addressId);
	boolean addAddress(Address address);
	boolean updateAddress(Address address);
	Address getBillingAddress(int userId);
	List<Address> listShippingAddresses(int userId);
	
	List<User> listUser(String userEmail);
	

	
}
