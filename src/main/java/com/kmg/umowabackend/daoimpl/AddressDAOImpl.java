package com.kmg.umowabackend.daoimpl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kmg.umowabackend.dao.AddressDAO;
import com.kmg.umowabackend.dto.Address;

@Repository("addressDAO")
@Transactional
public class AddressDAOImpl implements AddressDAO{

	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public Address get(int addressId) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Address getByUserId(int userId) {
		String query = "FROM Address WHERE userId = " + userId;
		
		return sessionFactory
				.getCurrentSession()
					.createQuery(query, Address.class)
						.getSingleResult();
	}
	
	@Override
	public List<Address> getAddressUserId(int userId) {
		String query = "FROM Address WHERE userId = " + userId;
		
		return sessionFactory
				.getCurrentSession()
					.createQuery(query, Address.class)
						.getResultList();
	}

	@Override
	public List<Address> list() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean add(Address address) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean update(Address address) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(Address address) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Address> getAdresByParam(String param, int count) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Address> listActiveAdresy() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Address> listUserAdresy(int userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Address> listUserAdresySprzedajacy(String email) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Address> listActiveAdresyByCategory(int categoryId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Address> getLatestActiveAdresy(int count) {
		// TODO Auto-generated method stub
		return null;
	}

}
