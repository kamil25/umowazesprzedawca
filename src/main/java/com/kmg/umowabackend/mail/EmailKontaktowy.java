package com.kmg.umowabackend.mail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.File;
import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
 

public class EmailKontaktowy {
     
	 private String host = "";
     private int port = 0;
     private String username = "";
     private String password = "";
     
     private String imieKlienta = "";
     private String emailKlienta = "";
     private String trescWiadomosci = "";
     private String emailSprzedajacy = "";
     private int idumowy;
     
     public EmailKontaktowy(String host, int port, String username, String password, String imieKlienta, String emailKlienta, String trescWiadomosci) {
         this.host = host;
         this.port = port;
         this.username = username;
         this.password = password;
         this.imieKlienta = imieKlienta;
         this.emailKlienta= emailKlienta;
         this.trescWiadomosci = trescWiadomosci;
        
         
         sendMail();
     }
     
     private void sendMail() {
        Properties prop = new Properties();
        prop.put("mail.smtp.auth", true);
        prop.put("mail.smtp.starttls.enable", "true");
        prop.put("mail.smtp.host", host);
        prop.put("mail.smtp.port", port);
        prop.put("mail.smtp.ssl.trust", host);
        Session session = Session.getInstance(prop, new Authenticator() {
     
        @Override
        protected PasswordAuthentication getPasswordAuthentication() {
                
    	return new PasswordAuthentication(username, password);
            }
        });
         
      
        try {
                 Message message = new MimeMessage(session);
                 message.setFrom(new InternetAddress("umowazesprzedawca@gmail.com"));
                 message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("umowazesprzedawca@gmail.com"));
                 message.setSubject("Nowa wiadomosc z formularza kontaktowego" + emailKlienta);
                 String msg = "wiadomosc od: " + imieKlienta + " " +emailKlienta + "o treści: " + trescWiadomosci;
                 MimeBodyPart mimeBodyPart = new MimeBodyPart();
                 mimeBodyPart.setContent(msg, "text/html");
                 //MimeBodyPart attachmentBodyPart = new MimeBodyPart();
                 //attachmentBodyPart.attachFile(new File("pom.xml"));
                 Multipart multipart = new MimeMultipart();
                 multipart.addBodyPart(mimeBodyPart);
                 //multipart.addBodyPart(attachmentBodyPart);
                 message.setContent(multipart);
                 Transport.send(message);
         } catch (Exception e) {
            e.printStackTrace();
        }
    }
     						
     public static void main(String imieKlienta, String emailKlienta, String trescWiadomosci) {
    	 String imieKlienta2 = imieKlienta;
    	 String emailKlienta2 = emailKlienta;
    	 String trescWiadomosci2 = trescWiadomosci;
    	 
    	 
    	 
        new EmailKontaktowy("smtp.gmail.com", 587, "umowazesprzedawca@gmail.com", "Gevel1990", imieKlienta, emailKlienta, trescWiadomosci);
            
    }
 }