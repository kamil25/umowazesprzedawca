package com.kmg.umowabackend.mail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.File;
import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
 

public class EmailService {
     
	 private String host = "";
     private int port = 0;
     private String username = "";
     private String password = "";
     
     private String email = "";
     private String code = "";
     
     public EmailService(String host, int port, String username, String password, String email, String code) {
         this.host = host;
         this.port = port;
         this.username = username;
         this.password = password;
         this.email = email;
         this.code = code;
         
         sendMail();
     }
     
     private void sendMail() {
        Properties prop = new Properties();
        prop.put("mail.smtp.auth", true);
        prop.put("mail.smtp.starttls.enable", "true");
        prop.put("mail.smtp.host", host);
        prop.put("mail.smtp.port", port);
        prop.put("mail.smtp.ssl.trust", host);
        Session session = Session.getInstance(prop, new Authenticator() {
     
        @Override
        protected PasswordAuthentication getPasswordAuthentication() {
                
    	return new PasswordAuthentication(username, password);
            }
        });
         
      
        try {
                 Message message = new MimeMessage(session);
                 message.setFrom(new InternetAddress("umowazesprzedawca@gmail.com"));
                 message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
                 message.setSubject("Rejestracja www.UmowaZeSprzedawca.pl");
                 String msg = "Dzi&#281;kujemy za rejestracj&#281; w serwisie www.UmowaZeSprzedawca.pl. "
                 		+ "Aby doko&#324;czy&#263; proces rejestracji wejd&#378; na www.UmowaZeSprzedawca.pl/rejestracja/potwierdzenie i wpisz kod:"
                		 + " " + code;
                 MimeBodyPart mimeBodyPart = new MimeBodyPart();
                 mimeBodyPart.setContent(msg, "text/html");
                 //MimeBodyPart attachmentBodyPart = new MimeBodyPart();
                 //attachmentBodyPart.attachFile(new File("pom.xml"));
                 Multipart multipart = new MimeMultipart();
                 multipart.addBodyPart(mimeBodyPart);
                 //multipart.addBodyPart(attachmentBodyPart);
                 message.setContent(multipart);
                 Transport.send(message);
         } catch (Exception e) {
            e.printStackTrace();
        }
    }
     
     public static void main(String email, String code) {
    	 String emailUser = email;
    	 String codeUser = code;
    	 
        new EmailService("smtp.gmail.com", 587, "umowazesprzedawca@gmail.com", "Gevel1990", emailUser, codeUser);
    }
 }